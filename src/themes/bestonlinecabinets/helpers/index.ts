import config from 'config'
import { currentStoreView } from '@vue-storefront/core/lib/multistore'
import { router } from '@vue-storefront/core/app'

export function getPathForStaticPage (path: string) {
  const { storeCode } = currentStoreView()
  const isStoreCodeEquals = storeCode === config.defaultStoreCode

  return isStoreCodeEquals ? `/i${path}` : path
}

export function getCategoryUrl (path: string) {
  const catMageUrl = config.bocm.categoryImage.url
  const defaultImage = config.bocm.categoryImage.default
  return path ? catMageUrl + path : defaultImage
}

export function getSliderUrl (path: string) {
  const sliderUrl = config.bocm.sliderImage.url
  const defaultImage = config.bocm.sliderImage.default
  return path ? sliderUrl + path : defaultImage
}

export function getSliderPdfUrl (path: string) {
  const pdfUrl = config.bocm.sliderImage.pdfUrl
  const defaultImage = config.bocm.sliderImage.default
  return path ? pdfUrl + path : defaultImage
}

export function getVideoData (url: string) {
  if (url) {
    const id = url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
    let type = null
    if (id[3].indexOf('youtu') > -1) {
      type = 'youtube';
    } else if (id[3].indexOf('vimeo') > -1) {
      type = 'vimeo';
    } else {
      throw new Error('Video URL not supported.');
    }
    let videoId = id[6];
    return {
      type: type,
      id: videoId
    }
  }
  return null;
}
export function replaceAnchorTag (refs) {
  var links = refs.querySelectorAll('a')
  for (var i = 0; i < links.length; i++) {
    links[i].addEventListener('click', (event) => {
      var target = event.target
      while (target) {
        if (target instanceof HTMLAnchorElement) {
          var link = target.getAttribute('href')
          if (link.substr(0, 4) === 'http' || link.substr(0, 3) === 'tel' || link.substr(0, 6) === 'mailto') {
            window.location.href = link
          } else {
            router.push(target.getAttribute('href'))
          }
          break
        }
        target = target.parentNode
      }
      event.preventDefault()
    })
  }
}
