import { Module } from 'vuex'
import BocmCategoryState from '../types/BocmCategoryState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const BocmCategoryModuleStore: Module<BocmCategoryState, any> = {
  namespaced: true,
  state: {
    filters: [],
    subCategory: []
  },
  mutations,
  actions,
  getters
}
