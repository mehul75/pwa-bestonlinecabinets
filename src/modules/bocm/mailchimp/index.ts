import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { mailchimpModule } from './store'

export const KEY = 'mailchimp'

export const MailchimpModule: StorefrontModule = function ({ store }) {
  store.registerModule(KEY, mailchimpModule)
}
