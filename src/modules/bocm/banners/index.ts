import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { BannerModuleStore } from './store/banner/index';

export const BannerModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('banner', BannerModuleStore)
};
