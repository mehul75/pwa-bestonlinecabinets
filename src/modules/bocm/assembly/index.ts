import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { AssemblyModuleStore } from './store/index';

export const AssemblyModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('assembly', AssemblyModuleStore)
};
