import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { ProductQuestionModuleStore } from './store/index';

export const ProductQuestionModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('productQuestion', ProductQuestionModuleStore)
};
