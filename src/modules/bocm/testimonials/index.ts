import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { TestimonialsModuleStore } from './store/index';

export const TestimonialsModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('testimonials', TestimonialsModuleStore)
};
