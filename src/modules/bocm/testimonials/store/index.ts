import { Module } from 'vuex'
import TestimonialState from '../types/TestimonialState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const TestimonialsModuleStore: Module<TestimonialState, any> = {
  namespaced: true,
  state: {
    testimonials: []
  },
  mutations,
  actions,
  getters
}
