import { ActionTree } from 'vuex';
import TestimonialState from '../types/TestimonialState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import { SearchQuery } from 'storefront-query-builder'

const entityName = 'testimonials'
const actions: ActionTree<TestimonialState, any> = {
  /**
   * Retrieve groups
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  list (context, { filterValues = null, filterField = null, skipCache = false }) {
    let query = new SearchQuery();
    if (filterValues) {
      query = query.applyFilter({
        key: filterField,
        value: { eq: filterValues }
      });
    }
    if (skipCache || !context.state.testimonials || context.state.testimonials.length === 0) {
      return quickSearchByQuery({
        query,
        entityType: entityName
      })
        .then(resp => {
          context.commit(types.FETCH_TESTIMONIALS, resp.items);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'Testimonials')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.testimonials;
        resolve(resp);
      });
    }
  }
};
export default actions;
