import { required, email, minLength, sameAs } from 'vuelidate/lib/validators'
import { Reviews } from '@vue-storefront/core/modules/review/components/Reviews'
import BaseInput from 'theme/components/core/blocks/Form/BaseInput'
import BaseTextarea from 'theme/components/core/blocks/Form/BaseTextarea'
import ButtonFull from 'theme/components/theme/ButtonFull'
import BaseCheckbox from 'theme/components/core/blocks/Form/BaseCheckbox.vue'
import NoSSR from 'vue-no-ssr'
import i18n from '@vue-storefront/i18n'

export const AlReviews = {
  name: 'AlReviews',
  data () {
    return {
      reviewImages: [],
      formData: {
        name: '',
        summary: '',
        review: '',
        terms: false,
        ratings: ''
      }
    }
  },
  mixins: [ Reviews ],
  validations: {
    formData: {
      name: {
        minLength: minLength(2),
        required
      },
      summary: {
        required
      },
      review: {
        required
      },
      ratings: {
        required
      },
      terms: {
        sameAs: sameAs(() => true)
      }
    }
  },
  components: {
    ButtonFull,
    BaseInput,
    BaseTextarea,
    BaseCheckbox,
    'no-ssr': NoSSR
  },
  computed: {
    currentUser () {
      return this.$store.state.user.current
    }
  },
  methods: {
    reviewImageUpload (event) {
      const input = event.target
      if (input.files && input.files.length) {
        this.reviewImages = [];
        let fileData = input.files
        const errorFile = [...fileData].filter(f => !f.type.includes('image/jpg') && !f.type.includes('image/jpeg') && !f.type.includes('image/png'))
        if (errorFile.length > 0) {
          if (errorFile.length === 1) {
            const errorFileName = errorFile[0].name
            this.$store.dispatch('notification/spawnNotification', {
              type: 'warning',
              message: errorFileName + this.$t(' is not allowed! Only (jpg, png, jpeg) format\'s are allowed.'),
              action1: { label: this.$t('OK') }
            })
            return
          }
          this.$store.dispatch('notification/spawnNotification', {
            type: 'warning',
            message: this.$t('Some file types are not allowed! Only (jpg, png, jpeg) format\'s are allowed.'),
            action1: { label: this.$t('OK') }
          })
          return
        }
        for (var key in fileData) {
          if (fileData.hasOwnProperty(key)) {
            const k = parseInt(key)
            var reader = new FileReader();
            reader.onload = (e) => {
              const base64data = e.target.result
              const base64Ary = base64data.split(',');
              let appendData = {
                base64_encoded_data: base64Ary.length > 0 ? base64Ary[1] : base64data,
                type: fileData[k].type,
                name: fileData[k].name
              }
              this.reviewImages.push(appendData);
            }
            reader.readAsDataURL(fileData[key]);
          }
        }
      }
    },
    validate () {
      if (!this.currentUser) {
        this.login();
        return false;
      }
      this.$v.$touch()
      if (!this.$v.$invalid) {
        this.submit()
      }
    },
    refreshList () {
      const getProductId = this.product.type_id === 'configurable' ? this.product.parentId : this.product.id
      this.$store.dispatch('review/list', { productId: getProductId, size: 500, sort: 'created_at:desc' })
    },
    async submit () {
      const reviewArgs = {
        'product_id': this.product.type_id === 'configurable' ? this.product.parentId : this.product.id,
        'title': this.formData.summary,
        'detail': this.formData.review,
        'nickname': this.formData.name,
        'review_entity': 'product',
        'customer_id': this.currentUser ? this.currentUser.id : null,
        'ratings': [{
          'rating_name': 'Rating',
          'value': this.formData.ratings
        }]
      }
      if (this.formType === 'rating-images') {
        reviewArgs.images = this.reviewImages
      }
      const isReviewCreated = await this.$store.dispatch('review/add', reviewArgs)

      if (isReviewCreated) {
        this.$store.dispatch('notification/spawnNotification', {
          type: 'success',
          message: i18n.t('You submitted your review for moderation.'),
          action1: { label: i18n.t('OK') }
        })
        return
      }

      this.$store.dispatch('notification/spawnNotification', {
        type: 'error',
        message: i18n.t('Something went wrong. Try again in a few seconds.'),
        action1: { label: i18n.t('OK') }
      })
    },
    clearReviewForm () {
      this.formData.name = ''
      this.formData.summary = ''
      this.formData.review = ''
      this.formData.ratings = ''
      this.reviewImages = []
      this.$v.$reset()
    },
    login () {
      this.$bus.$emit('modal-show', 'modal-signup')
    },
    fillInUserData () {
      if (this.currentUser) {
        this.formData.name = this.currentUser.firstname
      }
    }
  },
  mounted () {
    this.$bus.$on('product-after-load', this.refreshList)
    this.$bus.$on('clear-add-review-form', this.clearReviewForm)
    this.$bus.$on('user-after-loggedin', this.fillInUserData)
  },
  destroyed () {
    this.$bus.$off('product-after-load', this.refreshList)
    this.$bus.$off('clear-add-review-form', this.clearReviewForm)
    this.$bus.$off('user-after-loggedin', this.fillInUserData)
  },
  beforeMount () {
    this.refreshList()
    this.fillInUserData()
  },
  props: {
    product: {
      type: Object,
      required: true,
      default: () => ({})
    },
    formType: {
      type: String,
      required: false,
      default: 'rating'
    }
  }
}
