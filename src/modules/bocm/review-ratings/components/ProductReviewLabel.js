export const ProductReviewLabel = {
  props: {
    product: {
      type: Object,
      required: true,
      default: () => ({})
    }
  },
  methods: {
    scrollToReviewsSection () {
      document.querySelector('.accordion-3').classList.add('is-open');
      const headerOffset = document.querySelector('.header').clientHeight;
      const offsetTop = document.querySelector('.accordion-3').offsetTop;
      const offset = offsetTop - headerOffset;
      scroll({
        top: offset,
        behavior: 'smooth'
      });
    }
  }
}
