import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.SET_HOSTED_FIELD_INSTANCE] (state, payload) {
    state.hostedFieldInstance = payload || false
  }
}
