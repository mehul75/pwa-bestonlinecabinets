import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { module } from './store';

export const Braintree: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('braintree', module)
};
