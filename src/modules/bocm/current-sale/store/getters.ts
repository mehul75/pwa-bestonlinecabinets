import CurrentSaleState from '../types/CurrentSaleState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<CurrentSaleState, any> = {
  getGroupsList: (state) => state.groups
}
