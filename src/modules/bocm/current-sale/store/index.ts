import { Module } from 'vuex'
import CurrentSaleState from '../types/CurrentSaleState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const CurrentSaleModuleStore: Module<CurrentSaleState, any> = {
  namespaced: true,
  state: {
    groups: []
  },
  mutations,
  actions,
  getters
}
