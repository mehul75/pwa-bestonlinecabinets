import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.FETCH_GROUPS] (state, groups) {
    state.groups = groups || []
  }
}
