import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { StorePaymentModuleStore } from './store/index';

export const StorePaymentModule: StorefrontModule = function ({
  store
}) {
  store.registerModule('storePayment', StorePaymentModuleStore)
};
