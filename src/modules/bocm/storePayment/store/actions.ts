import { ActionTree } from 'vuex';
import StorePaymentState from '../types/storePaymentState';
import * as types from './mutation-types';
import rootStore from '@vue-storefront/core/store'
import { Logger } from '@vue-storefront/core/lib/logger';
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'

const actions: ActionTree<StorePaymentState, any> = {
  async storedPaymentMethodList ({ commit }) {
    try {
      let url = rootStore.state.config.bocm.storePaymentMethod.paymentMethodList.endpoint
      const currentUser = rootStore.state.user.current
      let currentUserId = (currentUser) ? currentUser.id : ''

      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }
      return await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ customer_id: currentUserId })
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            commit(types.FETCH_PAYMENT_METHOD_LIST, data.result.credit_card_info)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'storedPaymentMethodList')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.', 'storedPaymentMethodList')()
    }
  },
  async savePaymentMethod ({ commit }, paymentMethodData) {
    try {
      let url = rootStore.state.config.bocm.storePaymentMethod.savePaymentMethod.endpoint

      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }
      return await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(paymentMethodData)
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            if (data.result.length) {
              return data.result[0]
            }
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'savePaymentMethod')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.' + e, 'savePaymentMethod')()
    }
  },
  async removePaymentMethod ({ commit }, cardData) {
    try {
      let url = rootStore.state.config.bocm.storePaymentMethod.removePaymentMethod.endpoint

      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }
      return await fetch(url, {
        method: 'DELETE',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ cardData })
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            return data.result
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'savePaymentMethod')()
          }
        })
    } catch (e) {
      Logger.error('Something went wrong. Try again in a few seconds.' + e, 'savePaymentMethod')()
    }
  }
};

export default actions;
