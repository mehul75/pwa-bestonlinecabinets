import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { BocmproductModuleStore } from './store/index';

export const BocmproductModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('bocmproduct', BocmproductModuleStore)
};
