import { ActionTree } from 'vuex'
import * as types from './mutation-types'
import RootState from '@vue-storefront/core/types/RootState'
import CategoryCompareState from '../types/CategoryCompareState'
import { Logger } from '@vue-storefront/core/lib/logger'
import config from 'config'
import { quickSearchByQuery } from '@vue-storefront/core/lib/search'
import { SearchQuery } from 'storefront-query-builder'

const actions: ActionTree<CategoryCompareState, RootState> = {
  async load ({ commit, getters, dispatch }, force: boolean = false) {
    if (force || !getters.isCompareLoaded) {
      commit(types.SET_COMPARE_LOADED)
      const storedItems = await dispatch('fetchCurrentCompare')
      if (storedItems) {
        commit(types.COMPARE_LOAD_COMPARE, JSON.parse(storedItems))
        Logger.info('Compare state loaded from browser cache: ', 'cache', storedItems)()
      }
    }
  },
  async fetchCompareCategoryAttribute ({commit}) {
    var attributeList = [];
    config.entities.category.compareAttributes.forEach(function(obj){
      attributeList.push(obj.code);
    })
    let searchQuery = new SearchQuery()
        searchQuery = searchQuery.applyFilter({ key: 'attribute_code', value: { 'in': attributeList }  })
        await quickSearchByQuery({ 
            entityType: 'category_attributes', 
            query: searchQuery
        }).then(resp => {
            commit(types.FETCH_COMPARE_CATEGORY_ATTRIBUTES, resp.items)
        })
  }, 
  async fetchCurrentCompare () {
    return localStorage.getItem('category-compare')
  },
  async addItem ({ commit }, category) {
    commit(types.COMPARE_ADD_ITEM, { category })
  },
  async removeItem ({ commit }, category) {
    commit(types.COMPARE_DEL_ITEM, { category })
  },
  async clear ({ commit }) {
    commit(types.COMPARE_LOAD_COMPARE, [])
  }
}

export default actions
