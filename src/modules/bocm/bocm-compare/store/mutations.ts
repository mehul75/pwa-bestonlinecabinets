import { MutationTree } from 'vuex'
import * as types from './mutation-types'
import CategoryCompareState from '../types/CategoryCompareState'

const mutations: MutationTree<CategoryCompareState> = {
  /**
   * Add category to Compare
   * @param {Object} category data format for categorys is described in /doc/ElasticSearch data formats.md
   */
  [types.COMPARE_ADD_ITEM] (state, { category }) {
    const record = state.items.find(c => c.id === category.id)
    if (!record) {
      state.items.push(category)
      localStorage.setItem('category-compare', JSON.stringify(state.items))
    }
  },
  [types.COMPARE_DEL_ITEM] (state, { category }) {
    state.items = state.items.filter(c => c.id !== category.id)
    localStorage.setItem('category-compare', JSON.stringify(state.items))
  },
  [types.COMPARE_LOAD_COMPARE] (state, storedItems) {
    state.items = storedItems || []
    localStorage.setItem('category-compare', JSON.stringify(state.items))
  },
  [types.SET_COMPARE_LOADED] (state, isLoaded: boolean = true) {
    state.loaded = isLoaded
  },
  [types.FETCH_COMPARE_CATEGORY_ATTRIBUTES] (state, attributes) {
    state.compareAttributeList = attributes
  }
  
}

export default mutations
