import { GetterTree } from 'vuex'
import RootState from '@vue-storefront/core/types/RootState'
import CategoryCompareState from '../types/CategoryCompareState'

const getters: GetterTree<CategoryCompareState, RootState> = {
  isEmpty: state => state.items.length === 0,
  isOnCompare: state => category => state.items.some(c => c.id === category.id),
  isCompareLoaded: state => state.loaded,
  getCompareProductsCount: state => state.items.length,
  getCompareItems: state => state.items,
  getCompareCategoryAttributeList: state => state.compareAttributeList
}

export default getters
