import { Module } from 'vuex'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'
import RootState from '@vue-storefront/core/types/RootState'
import CategoryCompareState from '../types/CategoryCompareState'

export const categoryCompareStore: Module<CategoryCompareState, RootState> = {
  namespaced: true,
  state: {
    loaded: false,
    items: [],
    compareAttributeList: []
  },
  getters,
  actions,
  mutations
}
