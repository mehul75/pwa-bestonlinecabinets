
import { categoryCompareStore } from './store'
import { StorefrontModule } from '@vue-storefront/core/lib/modules';

export const CategoryCompareModule: StorefrontModule = function ({ store }) {
  store.registerModule('categoryCompare', categoryCompareStore)
}
